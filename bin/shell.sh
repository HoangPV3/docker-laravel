#!/bin/bash

if [  -z "$1"  ]
	then 
		docker-compose exec -uadmin php bash
	else
		docker-compose exec -u $@ php bash
fi